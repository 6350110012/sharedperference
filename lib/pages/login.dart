import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  const Login({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<Login> createState() => _LoginState();
}


class _LoginState extends State<Login> {
  late SharedPreferences prefs;
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  String name = "";

  get assets => null;

  void initState() {
    super.initState();
    _dataLogin();
  }

  void _dataLogin() async {
    prefs = await SharedPreferences.getInstance();
    name = prefs.getString("Email") ?? '';
    setState(() {
      email.text = name;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("LOG IN",style: TextStyle(fontSize: 20,color:Colors.white),),
      ),
      body: Container(
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 30,width: 20,
                    ),
                    Image.asset(
                      'assets/logoflutter.png',
                      width: 250,
                      height: 220,

                    ),

                    Padding(
                      padding: const EdgeInsets.only(right: 295,bottom: 10,top: 40),
                      child: Text(
                        "E-MAIL",
                        style: TextStyle(
                            fontSize: 16, color: Colors.black45,),
                      ),

                    ),

                    textfield(
                      data: email,
                      n: '',
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 260,bottom: 10,top: 20),
                      child: Text(
                        "PASSWORD",
                        style: TextStyle(
                          fontSize: 16, color: Colors.black45,),
                      ),

                    ),

                    textfield(
                      data: password,
                      n: 'Password',
                    ),

                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 25, 0, 25),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          ButtonTheme(
                            minWidth: 200.0,
                            height: 60.0,
                            // ignore: deprecated_member_use
                            child: RaisedButton(
                              onPressed: () {
                                login();
                              },
                              child: Text(
                                "LOG IN",
                                style: TextStyle(
                                    fontSize: 20, color: Colors.white),
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0)),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  login() async {
    prefs = await SharedPreferences.getInstance();
    prefs.setString("Email", email.text.toString());
  }
}

class textfield extends StatelessWidget {
  const textfield({
    Key? key,
    required this.data,
    required this.n,
  }) : super(key: key);

  final TextEditingController data;
  final String n;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: data,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: n,
        hintText: ' $n',
      ),
    );
  }
}
